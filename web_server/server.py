###############################################################################
#
# The MIT License (MIT)
#
# Copyright (c) Tavendo GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
###############################################################################

import sys

from twisted.internet import reactor
from twisted.python import log
from twisted.web.server import Site
from twisted.web.static import File

from twisted.internet.defer import inlineCallbacks, returnValue

from autobahn.twisted.websocket import WebSocketServerFactory, \
    WebSocketServerProtocol

from autobahn.twisted.resource import WebSocketResource

import zerorpc
import json

@inlineCallbacks
def processor_nlp_call(payload):
    """ make a call to the Processor  """
    print str(payload)
    c = zerorpc.Client()
    print "connecting to client"
    c.connect("tcp://127.0.0.1:4245")
    r = yield c.nlp_query(payload)
    res = str(r)
    returnValue(res)


@inlineCallbacks
def update_servers_call():
    """ make a call to the Processor  """
    c = zerorpc.Client()
    c.connect("tcp://127.0.0.1:4245")
    r = yield c.retrieve_servers()
    res = str(r)
    print res
    returnValue(res)



class EchoServerProtocol(WebSocketServerProtocol):

    

    def onConnect(self, request):
        # On Connect, retrieve list of servers
        print("WebSocket connection request: {}".format(request))

    @inlineCallbacks
    def onMessage(self, payload, isBinary):
        print "message recieved"
        data = json.loads(payload)
        print data
        if data['type'] == "server_request":
            res = yield update_servers_call()
        else:
            res = yield processor_nlp_call(payload)
        print res
       # echo back message verbatim
        self.sendMessage(res, isBinary)


if __name__ == '__main__':

    if len(sys.argv) > 1 and sys.argv[1] == 'debug':
        log.startLogging(sys.stdout)
        debug = True
    else:
        debug = False

    factory = WebSocketServerFactory(u"ws://127.0.0.1:8081")
    
    factory.protocol = EchoServerProtocol

    resource = WebSocketResource(factory)

    # we server static files under "/" ..
    root = File("site")

    # and our WebSocket server under "/ws"
    root.putChild(u"ws", resource)

    # both under one Twisted Web Site
    site = Site(root)
    reactor.listenTCP(8081, site)

    reactor.run()
