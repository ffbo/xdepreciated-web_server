
    function processGeppettoLink(data) {
        
           // Wipe the iframe
            //load new one        
            site = 'http://'+data['server']+'/org.geppetto.frontend/geppetto?load_project_from_url=SERVER_ROOT/app_data/'+data['geppetto_name']+'/Specification.json'
         document.getElementById('morphology').src = site;   
    }

    function processServerLists(data) {
        
           // load the nlp servers
            select = document.getElementById('nlp_servers');
            select.innerHTML = ""
		    l = data["nlp_servers"];
            var i; 
            for (i = 0; i < l.length; ++i) {
                var opt = document.createElement('option');
                opt.value = l[i];
                opt.innerHTML = l[i];
                select.appendChild(opt);
            }

                
           // load the na servers
            select = document.getElementById('na_servers');
            select.innerHTML = ""
		    l = data["na_servers"];
            var i; 
            for (i = 0; i < l.length; ++i) {
                var opt = document.createElement('option');
                opt.value = l[i];
                opt.innerHTML = l[i];
                select.appendChild(opt);
            }
           // load the vis servers
            select = document.getElementById('vis_servers');
            select.innerHTML = ""
		    l = data["vis_servers"];
            var i; 
            for (i = 0; i < l.length; ++i) {
                var opt = document.createElement('option');
                opt.value = l[i];
                opt.innerHTML = l[i];
                select.appendChild(opt);
            }
    }


    function processIncomingMessage(data) {
        if ("error" in data){
            window.alert(data['error']);
            console.log(data)
        } else if ("server_lists" in data){
            processServerLists(data["server_lists"])
        } else {
        
            processGeppettoLink(data)
        }
    }

         window.onload = function() {
    
            ellog = document.getElementById('log');

            var wsuri;
            if (window.location.protocol === "file:") {
               wsuri = "ws://127.0.0.1:8081/ws?a=23&foo=bar";
            } else {
               wsuri = "ws://" + window.location.hostname + ":8081/ws";
            }

            if ("WebSocket" in window) {
               sock = new WebSocket(wsuri);
            } else if ("MozWebSocket" in window) {
               sock = new MozWebSocket(wsuri);
            } else {
               log("Browser does not support WebSocket!");
            }

            if (sock) {
               sock.onopen = function() {
                  log("Connected to " + wsuri);
                  send_server_request()
                  
               }

               sock.onclose = function(e) {
                  log("Connection closed (wasClean = " + e.wasClean + ", code = " + e.code + ", reason = '" + e.reason + "')");
                  sock = null;
               }

               sock.onmessage = function(e) {
                  // Log Incoming Data
                  log("Got echo: " + e.data);
                  
                  // Parse JSON 
                  query = JSON.parse(e.data);
    
                  // Pass Message on to be processed
                  processIncomingMessage(query);
               }
            }
         };

         function send_query(section) {
            var msg = {};
            msg['type'] = 'nlp_query'

            var sel = document.getElementById("nlp_servers");
            msg['nlp_server'] = sel.options[sel.selectedIndex].text;
            var sel = document.getElementById("na_servers");
            msg['na_server'] = sel.options[sel.selectedIndex].text;
            var sel = document.getElementById("vis_servers");
            msg['vis_server'] = sel.options[sel.selectedIndex].text;

            msg['nlp_query'] = document.getElementById('srch_'.concat(section)).value;            
            send(msg);
         };

        function send_server_request() {
            var msg = {};
            msg['type'] = 'server_request'  
            send(msg);
         };

         function send(msg) {
            if (sock) {
                sock.send(JSON.stringify(msg));
                log("Sent : " + JSON.stringify(msg));
            } else {
               log("Not connected.");
            }
         };


         function log(m) {
            ellog.innerHTML += m + '\n';
            ellog.scrollTop = ellog.scrollHeight;
         };

